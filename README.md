# Screen Design & HTML/CSS

Die Inhalte in diesem Ordner:

HTML Dokumente zum der HTML Vorlesung & Übung folgen.

Im Ordner [Slides](https://gitlab.com/Kuchengnom/html-anfaenger/tree/master/Slides) sind die Folien der Vorlesungen.

## Aktuelle Dateien:

[23.html](https://gitlab.com/Kuchengnom/html-anfaenger/blob/master/23.html)
### CSS am Samstag morgen
![CatDog](https://media.giphy.com/media/1j0LVA9U7k8yFz0s/giphy.gif)

I’m dying inside

## Klausur

[x] Multiple Choice

### Referenzen:

- Screendesign Folien

- HTML Grundlegendes wie in 00.html - 04.html
- Grid ist Optional
- Wiederholung 20-22
- 23.html Extra( Wichtig Tabellen und so)

### Themen zum Googeln:
- [W3scools tutorial HTML](https://www.w3schools.com/html/default.asp)
- Google Fonts Einbindung (extrem Stylesheets)
- Das Box Modell
